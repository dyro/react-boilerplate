module.exports = {
  entry: './src/js/app.js',

  output: {
    filename: 'public/bundle.js',
    path: __dirname
  },

  devtool: 'eval',

  module: {
    loaders: [
      {test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/}
    ]
  }
};
