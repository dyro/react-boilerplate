var server = require('pushstate-server');

var config = {
  port: process.env.PORT || 6789,
  directory: './public'
};

server.start(config);