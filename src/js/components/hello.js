import React from 'react'

export default class Hello extends React.Component {
	constructor (props) {
    super(props)
    this.state = {person: props.person};
  }

  render () {
    return <h1>Hello, {this.state.person}</h1>
  }
}

Hello.propTypes = {person: React.PropTypes.string.isRequired};
// Hello.defaultProps = {}