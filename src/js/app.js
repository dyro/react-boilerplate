import React from 'react';
import Hello from './components/hello';

const reactContainer = document.getElementById('container');
const people = ['George', 'Bill', 'Barack'];

class App extends React.Component {
  render () {
    return <div>{people.map(person => <Hello person={person}/>)}</div>
  }
}

React.render(<App/>, reactContainer);