# React Boilerplate

### Contents
* [webpack](https://github.com/webpack/webpack)
* [pushstate-server](https://github.com/scottcorgan/pushstate-server)
* [babel(formely 6to5)](https://github.com/babel/babel)
* [react](https://github.com/facebook/react)

### Commmands
* `node server.js` will start the push state server on port `6789` as will `npm start`
* `webpack` will compile all assets to `public/bundle.js`.
* `webpack --watch` will compile assets on file changes.

### How to Use
```sh
$ git clone https://gitlab.com/dyro/react-boilerplate.git [app-name]
$ cd [app-name]
$ npm install
$ npm start
$ webpack --watch
```
Then go to `http://localhost:6789`